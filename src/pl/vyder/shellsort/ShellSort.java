package pl.vyder.shellsort;

import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

public class ShellSort {

    /**
     * Konfiguracja:
     * quantity - ilość elementów tabeli
     * max - maksymalna wartość elementu tabeli
     * min - minimalna wartość elementu tabeli
     * loopsForEachSequenceType - ilość powtórzeń sortowania dla każdego typu ciągu odstępów
     * filePath - ścieżka do pliku CVS na dysku
     * fileName - nazwa pliku CSV
     * fileExtension - rozszerzenie pliku
     * isWriteOutputToFile - true -> zapisujemy wyniki sortowania do serii plików
     */
    private int quantity = 1000000;
    private int max = 1000000;
    private int min = 1;
    private static int loopsForEachSequenceType = 10;
    private String filePath = "c:/temp/";
    private String fileName = "shellSort";
    private String fileExtension = ".csv";
    private boolean isWriteOutputToFile = true;

    /**
     * Główna metoda do uruchomienia testu.
     *
     * Dla losowo wygenerowanego zbioru (o definiowanej liczbie elementów oraz ich zakresie)
     * wykonuje sortowanie wg zadanego ciągu odstępów, dla którego mierzy czas wykonania sortowania
     * i początkową oraz posortowaną tabelę z danymi eksportuje do pliku CSV.
     *
     */
    public static void main(String args[]) throws IOException {
        ShellSort shellSort = new ShellSort();

        int nums[] = shellSort.createDataToSort();

        // standardowy ciąg odsępów Shell'a
        for (int index = 0; index < loopsForEachSequenceType; index++) {
            int toSort[] = Arrays.copyOf(nums, nums.length);
            shellSort.testShell(shellSort, nums, toSort, index, GapSequence.SHELL);
        }

        // Frank, Lazarus, ciąg odstępów
        for (int index = 0; index < loopsForEachSequenceType; index++) {
            int toSort[] = Arrays.copyOf(nums, nums.length);
            shellSort.testShell(shellSort, nums, toSort, index, GapSequence.FRANK_LAZARUS);
        }

        // ciąg odstępów Sadgewick'a
        for (int index = 0; index < loopsForEachSequenceType; index++) {
            int toSort[] = Arrays.copyOf(nums, nums.length);
            shellSort.testShell(shellSort, nums, toSort, index, GapSequence.SADGEWICK);
        }

        // ciąg odstępów - własny nr 1 - 2 * (N / 3^k)
        for (int index = 0; index < loopsForEachSequenceType; index++) {
            int toSort[] = Arrays.copyOf(nums, nums.length);
            shellSort.testShell(shellSort, nums, toSort, index, GapSequence.MY_GAP_1);
        }

        // ciąg odstępów - własny nr 2 - Fibonacci
        for (int index = 0; index < loopsForEachSequenceType; index++) {
            int toSort[] = Arrays.copyOf(nums, nums.length);
            shellSort.testShell(shellSort, nums, toSort, index, GapSequence.MY_GAP_2);
        }

        // ciąg odstępów - własny nr 3 - potęgi liczby 10
        for (int index = 0; index < loopsForEachSequenceType; index++) {
            int toSort[] = Arrays.copyOf(nums, nums.length);
            shellSort.testShell(shellSort, nums, toSort, index, GapSequence.MY_GAP_3);
        }

        // ciąg odstępów - własny nr 4
        for (int index = 0; index < loopsForEachSequenceType; index++) {
            int toSort[] = Arrays.copyOf(nums, nums.length);
            shellSort.testShell(shellSort, nums, toSort, index, GapSequence.MY_GAP_4);
        }

        // ciąg odstępów - własny nr 5
        for (int index = 0; index < loopsForEachSequenceType; index++) {
            int toSort[] = Arrays.copyOf(nums, nums.length);
            shellSort.testShell(shellSort, nums, toSort, index, GapSequence.MY_GAP_5);
        }

    }

    /**
     * Wywołanie metody sortującej z zadanymi danymi, z możliwością zapisu wyników sortowania do CSV.
     * Zapisuje w zależności od ustawionej flagi "isWriteOutputToFile".
     *
     *
     * @param shellSort - klasa sortująca
     * @param nums - tablica oryginalna, nieposortowana (do zapisania w CSV)
     * @param arrayToSort - tablica do posortowania
     * @param index - kolejna iteracja sortowania tej samej tablicy wejściowej
     * @param gapSequence - typ ciągu odstępów
     * @throws IOException
     */
    private void testShell(ShellSort shellSort, int[] nums, int[] arrayToSort, int index, GapSequence gapSequence) throws IOException {

        // przygotowanie odpowiedniego ciągu odstępów
        List<Integer> currentGapSequence = prepareGapSequence(arrayToSort.length, gapSequence);

        // pomiar czasu sortowania
        long start = System.currentTimeMillis();
        shellSort.shell(arrayToSort, currentGapSequence);
        long end = System.currentTimeMillis();

        System.out.println("Sorted Array with " + arrayToSort.length + " elements in " + (end-start) + " ms:");

        // zapis wyników do CSV
        if (isWriteOutputToFile) {
            shellSort.writeToCSV(nums, arrayToSort, (end - start), index, gapSequence, currentGapSequence);
        }
    }

    private List<Integer> prepareGapSequence(int length, GapSequence gapSequence) {
        List<Integer> currentGapSequence = Collections.emptyList();

        // przygotowanie odpowiedniego ciągu przyrostów
        switch(gapSequence) {
            case SHELL:
                currentGapSequence = shellStandardGapSequences(length);
                System.out.print(gapSequence + " will be used for sorting, ");
                for (int gap : currentGapSequence) {
                    System.out.print(gap + ", ");
                }
                break;
            case FRANK_LAZARUS:
                currentGapSequence = frankLazarusGapSequences(length);
                System.out.print(gapSequence + " will be used for sorting, ");
                for (int gap : currentGapSequence) {
                    System.out.print(gap + ", ");
                }
                break;
            case SADGEWICK:
                currentGapSequence = sadgewickGapSequences(length);
                System.out.print(gapSequence + " will be used for sorting, ");
                for (int gap : currentGapSequence) {
                    System.out.print(gap + ", ");
                }
                break;
            case MY_GAP_1:
                currentGapSequence = my_1_n3k_GapSequences(length);
                System.out.print(gapSequence + " will be used for sorting, ");
                for (int gap : currentGapSequence) {
                    System.out.print(gap + ", ");
                }
                break;
            case MY_GAP_2:
                currentGapSequence = my_2_fibonacci_GapSequence(length);
                System.out.print(gapSequence + " will be used for sorting, ");
                for (int gap : currentGapSequence) {
                    System.out.print(gap + ", ");
                }
                break;
            case MY_GAP_3:
                currentGapSequence = my_3_10k_GapSequence(length);
                System.out.print(gapSequence + " will be used for sorting, ");
                for (int gap : currentGapSequence) {
                    System.out.print(gap + ", ");
                }
                break;
            case MY_GAP_4:
                currentGapSequence = my_4_GapSequence(length);
                System.out.print(gapSequence + " will be used for sorting, ");
                for (int gap : currentGapSequence) {
                    System.out.print(gap + ", ");
                }
                break;
            case MY_GAP_5:
                currentGapSequence = my_5_GapSequence(length);
                System.out.print(gapSequence + " will be used for sorting, ");
                for (int gap : currentGapSequence) {
                    System.out.print(gap + ", ");
                }
                break;
            default:
                System.out.println(gapSequence + " not supported yet");
        }
        return currentGapSequence;
    }

    /**
     * Implementacja sortowania Shell'a z możliwością wyboru ciągu odstępów.
     *
     * @param a - tabela do posortowania
     * @param gapSequence - typ ciągu odstępów
     */
    private void shell(int[] a, List<Integer> gapSequence) {

        // jeśli podano prawidłowy ciąg przyrostów
        if (gapSequence.size() > 0) {
            for (int increment : gapSequence) {
                for (int i = increment; i < a.length; i++) {
                    int j = i;
                    int temp = a[i];
                    while (j >= increment && a[j - increment] > temp) {
                        a[j] = a[j - increment];
                        j = j - increment;
                    }
                    a[j] = temp;
                }
            }
        } else {
            // jeśli podany ciąg przyrostów jest pusty
            System.out.println("GAP SEQUENCE LIST IS EMPTY BECAUSE GAP SEQUENCE TYPE IS NOT IMPLEMENTED YET !!!");
            System.out.println("ARRAY WILL NOT BE SORTED !!!");
            //shell(a);
        }
    }

    /**
     * Podstawowa implementacja sortowania Shell'a.
     * Jest użyta wyłącznie w przypadku kiedy nie podano ciągu przyrostów.
     *
     * @param a - tabela do posortowania
     */
    private void shell(int[] a) {
        int increment = a.length / 2;
        while (increment > 0) {
            for (int i = increment; i < a.length; i++) {
                int j = i;
                int temp = a[i];
                while (j >= increment && a[j - increment] > temp) {
                    a[j] = a[j - increment];
                    j = j - increment;
                }
                a[j] = temp;
            }
            if (increment == 2) {
                increment = 1;
            } else {
                increment *= (5.0 / 11);
            }
        }
    }

    /**
     * Standardowy ciąg odstępów Shell'a  N / 2^k
     *
     * @param size - ilość elementów tablicy do posortowania
     * @return
     */
    private List<Integer> shellStandardGapSequences(int size) {
        List<Integer> gapSequences = new ArrayList<>();

        for (int k = 1; k < 1000; k++) {
            double orgResult = size / Math.pow(2, k);
            int result  = (int) Math.round(orgResult);
            //System.out.println(orgResult + " -> " + result);
            gapSequences.add(result);
            if (result == 1) {
                break;
            }
        }
        return gapSequences;
    }

    /**
     * Ciąg odstępów wg Frank'a & Lazarus'a: 2 * (N /(2^(k+1))) + 1
     *
     * @param size - ilość elementów tablicy do posortowania
     * @return
     */
    private List<Integer> frankLazarusGapSequences(int size) {
        List<Integer> gapSequences = new ArrayList<>();

        for (int k = 1; k < 1000; k++) {
            double orgResult = 2 * (size / Math.pow(2, k + 1)) + 1;
            int result  = (int) Math.round(orgResult);
            //System.out.println(orgResult + " -> " + result);
            gapSequences.add(result);
            if (result <= 3) {
                gapSequences.add(1);
                break;
            }
        }
        return gapSequences;
    }

    /**
     *
     *
     * @param size - ilość elementów tablicy do posortowania
     * @return
     */
    private List<Integer> sadgewickGapSequences(int size) {
        List<Integer> gapSequences = new ArrayList<>();

        gapSequences.add(1);

        for (int k = 1; k < 1000; k++) {
            double orgResult = Math.pow(4, k) + (3 * Math.pow(2, k - 1)) + 1;
            int result  = (int) Math.round(orgResult);
            //System.out.println(orgResult + " -> " + result);
            if (result > size) {
                break;
            }
            gapSequences.add(result);
        }
        Collections.reverse(gapSequences);
        return gapSequences;
    }

    /**
     * Ciąg wg wzoru: N / 3^k
     *
     * @param size - ilość elementów tablicy do posortowania
     * @return
     */
    private List<Integer> my_1_n3k_GapSequences(int size) { // 2 * (n / 3^k)
        List<Integer> gapSequences = new ArrayList<>();

        for (int k = 1; k < 1000; k++) {
            double orgResult = 2 * (size / Math.pow(3, k));
            int result  = (int) Math.round(orgResult);
            //System.out.println(orgResult + " -> " + result);
            gapSequences.add(result);
            if (result <= 3) {
                break;
            }
        }
        if (!gapSequences.contains(1)) {
            gapSequences.add(1);
        }
        return gapSequences;
    }

    /**
     * Ciąg Fibonacciego, na poterzeby sortowania zaczyna się od (1 ,2, 3, 5, ...)
     * zamiast od (0, 1, 1, 2, 3, ...)
     *
     * @param size - ilość elementów tablicy do posortowania
     * @return
     */
    private List<Integer> my_2_fibonacci_GapSequence(int size) {
        List<Integer> gapSequences = new ArrayList<>();

        // Set it to the number of elements you want in the Fibonacci Series
        int maxNumber = 100;
        int previousNumber = 1;
        int nextNumber = 2;

        for (int i = 1; i <= maxNumber; ++i) {
            if (previousNumber > size) {
                break;
            }
            gapSequences.add(previousNumber);

            int sum = previousNumber + nextNumber;
            previousNumber = nextNumber;
            nextNumber = sum;
        }
        Collections.reverse(gapSequences);
        if (!gapSequences.contains(1)) {
            gapSequences.add(1);
        }
        return gapSequences;
    }

    /**
     * Ciąg z wartościami 10 ^ k
     *
     * @param size - ilość elementów tablicy do posortowania
     * @return
     */
    private List<Integer> my_3_10k_GapSequence(int size) { // 10^k
        List<Integer> gapSequences = new ArrayList<>();

        for (int k = 0; k < 10; k++) {
            double orgResult = Math.pow(10, k);
            int result  = (int) Math.round(orgResult);
            //System.out.println(orgResult + " -> " + result);
            if (result >= size) {
                break;
            }
            gapSequences.add(result);
        }
        Collections.reverse(gapSequences);
        return gapSequences;
    }

    /**
     *
     * @param size - ilość elementów tablicy do posortowania
     * @return
     */
    private List<Integer> my_4_GapSequence(int size) { // 10^k
        List<Integer> gapSequences = new ArrayList<>();

        //TODO do zaimplementowania

        return gapSequences;
    }

    /**
     *
     * @param size - ilość elementów tablicy do posortowania
     * @return
     */
    private List<Integer> my_5_GapSequence(int size) { // 10^k
        List<Integer> gapSequences = new ArrayList<>();

        //TODO do zaimplementowania

        return gapSequences;
    }

    /**
     * Generowanie tabeli z losowymi wartościami
     * o zadanej liczbie elementów oraz zakresie każdego z nich.
     *
     * @return tabela z losowymi danymi
     */
    private int[] createDataToSort() {
        int range = max - min + 1;
        int[] output = new int[quantity];

        for (int i = 0; i < quantity; i++) {
            output[i] = (int)(Math.random() * range) + min;
        }
        return output;
    }

    /**
     * Metoda do generowania pliku CSV z tablicą wejściową, posortowaną oraz
     * z informacją o ilości elementów oraz czasie sortowania.
     *
     * @param inputArray - dane wejściowe, nieposortowane
     * @param sortedArray - dane wyjściowe, posortowane
     * @param time - czas sortowania
     *
     * @throws IOException
     */
    private void writeToCSV(int[] inputArray, int[] sortedArray, long time, int index, GapSequence gapSequenceType, List<Integer> gapSequence) throws IOException {

        // nie zapisujemy tablic, które nie były posortowane
        // (jeśli nie był podany ciąg przyrostów przy sortowaniu, to nie sortowaliśmy)
        if (gapSequence.size() > 0) {
            FileWriter csvWriter = new FileWriter(filePath + fileName + "-" + gapSequenceType.toString() + "-" + index + fileExtension);

            csvWriter.append("input array has elements :" + ";" + inputArray.length + "\n");
            csvWriter.append("and sorting took: " + ";" + time + " ms \n");
            csvWriter.append("gap sequence type: " + ";" + gapSequenceType + " " + gapSequenceType.description +  "  \n");
            csvWriter.append("gap sequence: " + ";" + gapSequence + " \n");
            csvWriter.append("lp." + ";" + "input array" + ";" + "sorted array" + "\n");

                for (int i = 0; i < inputArray.length; i++) {
                    csvWriter.append(i + ";" + inputArray[i] + ";" + sortedArray[i] + "\n");
                }

            csvWriter.flush();
            csvWriter.close();
        }
    }

    /**
     * Typy możliwych ciągów. Opisy w nawiasach użyte do prezentacji w CSV.
     */
    public enum GapSequence {
        SHELL("[Shell, 1959]"),
        FRANK_LAZARUS("[Frank, Lazarus, 1960]"),
        SADGEWICK("[Sadgewick, 1986]"),
        MY_GAP_1("[2 * (n / 3^k)]"),
        MY_GAP_2("[Fibonacci]"),
        MY_GAP_3("[potęgi liczby 10 -> 10^k]"),
        MY_GAP_4("[my gap 4]"),
        MY_GAP_5("[my gap 5]");

        private String description;

        GapSequence(String s) {
            this.description = s;
        }

        public String getDescription() {
            return description;
        }
    }

}
